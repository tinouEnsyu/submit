package frame;

/**
 * Created by seijihagawa on 2016/11/11.
 */

import java.util.*;

abstract class AIDemonProc {//全てのデモン手続きのスーパークラス（始まり）

    abstract
    public Object eval(
            AIFrameSystem inFrameSystem,
            AIFrame inFrame,
            String inSlotName,
            Iterator inSlotValues,
            Object inOpts);//抽象メソッドなので、このクラスのサブクラスはこのメソッドをオーバーライドしなければならない

    public Object eval(
            AIFrameSystem inFrameSystem,
            AIFrame inFrame,
            String inSlotName,
            Iterator inSlotValues) {
        return eval(inFrameSystem, inFrame, inSlotName, inSlotValues, null);
    }

}