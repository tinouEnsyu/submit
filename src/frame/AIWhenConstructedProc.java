package frame;

/**
 * Created by seijihagawa on 2016/11/11.
 */
public abstract class AIWhenConstructedProc {

    public boolean isWhenConstructedProc() {
        return true;
    }

    abstract public void eval(AIFrameSystem inFrameSystem, AIFrame inFrame);

}