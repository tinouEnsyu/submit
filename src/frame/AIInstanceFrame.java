package frame;

/**
 * Created by seijihagawa on 2016/11/11.
 */
public class AIInstanceFrame extends AIFrame {

    public AIInstanceFrame(
            AIFrameSystem inFrameSystem,
            AIClassFrame inSuperFrame,
            String inName) {
        super(inFrameSystem, inSuperFrame, inName, true);
    }

}