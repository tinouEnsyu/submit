package frame;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class DBpediaSearch {
	enum Mode{SUBJECT,PREDICATE,OBJECT}
	String dbpediaBaseUrl = "http://ja.dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fja.dbpedia.org&timeout=0&debug=on";
	String format = "text/csv";  
     // formatには，"text/csv" の他にも以下のような形式を指定可能
     //  "text/tsv" 
     //  "application/rdf+xml"
     //  "application/sparql-results+json"
     //  "text/html" 
	//課題5-3との兼ね合いは含んでいる単語がQandASystemのハッシュマップに含まれていた場合は5-3 とし、そうでなければ5-6とする
	public static void main(String args[]) {
        

        // 人物（foaf:Personクラス）のインスタンスを取得するSPARQLの例
        String sparql = 
            "SELECT * WHERE { "+// * を使ってパターンに含まれる全ての変数を得る
            "  ?person rdf:type foaf:Person;"+
            "          ?p       ?o."+
            "} LIMIT 10";

        //String url = dbpediaBaseUrl 
        //    + "&format=" + URLEncoder.encode(format)
         //   + "&query=" + URLEncoder.encode(sparql);

        //String result = getWebContent(url, "UTF-8");
        //System.out.println(result);
        
        new DBpediaSearch().getAnswerS(args[0]);
    }
	
	public ArrayList<String> getWordList(String arg_question){
		ArrayList<String> list = new ArrayList<String>();
		Sentence sentence = Sentence.parseTweet(arg_question).get(0);//係り受け解析を行う 1文の問題を想定しているので先頭だけを調べる
		int i = 0;
		while(i < sentence.size()){
			Chunk chunk_i = sentence.get(i);//i番目の文節を入手する
			int j = 0;
			String word = "";
			while(j < chunk_i.size()){
				Morpheme morph_j = chunk_i.get(j);//j番目の形態素を入手する
				if(morph_j.contain("名詞") && !morph_j.contain("代名詞") && !morph_j.contain("記号")){//代名詞でも記号でもない名詞を含んでいる場合
					word = word.concat(morph_j.getSurface());//繋げる
				}
				else{//名詞でない場合
					break;//脱出
				}
				++j;
			}
			if(word.length() > 0){
				list.add(word);
			}
			++i;
		}
		
		//System.out.println(list);
		return(list);
	}
	
	public String conv2Sparql(Mode arg_mode,String arg_word1,String arg_word2){
		String sparql = 
				"PREFIX dbpedia-ja: <http://ja.dbpedia.org/resource/>\n"+
				"PREFIX prop-ja: <http://ja.dbpedia.org/property/>\n"+
				"select distinct * where {";
		switch(arg_mode){
			case SUBJECT:{//主語を求めるモード
				sparql = sparql.concat("?s ");//主語を追加
				sparql = sparql.concat("prop-ja:"+arg_word1+" ");//述語を追加
				sparql = sparql.concat("dbpedia-ja:"+arg_word2+". ");//目的語を追加
				
				break;
			}
			case PREDICATE:{//述語を求めるモード
				sparql = sparql.concat("dbpedia-ja:"+arg_word1+" ");//主語を追加
				sparql = sparql.concat("?p ");//述語を追加
				sparql = sparql.concat("dbpedia-ja:"+arg_word2+". ");//目的語を追加
				break;
			}
			case OBJECT:{//目的語を求めるモード
				sparql = sparql.concat("dbpedia-ja:"+arg_word1+" ");//主語を追加
				sparql = sparql.concat("prop-ja:"+arg_word2+" ");//述語を追加
				sparql = sparql.concat("?o.");//目的語を追加
				break;
			}
		}
		sparql = sparql.concat("}LIMIT 20");
		//System.out.println(sparql);
		return(sparql);
	}
	
	public void getAnswerS(String arg_question){
		boolean find = false;
		System.out.println(arg_question);
		ArrayList<String> wordList = getWordList(arg_question);
		for(Mode mode:Mode.values()){//全てのモードで調べる
			for(String word1 : wordList){//単語全てのペアを調べる
				for(String word2 : wordList){
					if(!word1.equals(word2)){//同じ言葉のペアは調べない
						String query = conv2Sparql(mode,word1,word2);
						String url = dbpediaBaseUrl 
								+ "&format=" + URLEncoder.encode(format)
								+ "&query=" + URLEncoder.encode(query);
						ArrayList<String> result = getWebContentList(url, "UTF-8");
						if(result.size() > 0){
							find = true;
							for(String ans : result){
								System.out.println(ans);
							}
							
						}
					}
				}
			}
		}
		
		if(!find){
			System.out.println("すいません、分かりませんでした。");
		}
	}
	
	/**
     * 与えられたURLからHTML等のコンテンツを取得し，返す．
     * @param url 取得するコンテンツのURL
     * @param enc コンテンツの文字コード（UTF-8やEUC-JP, Shift_JISなど）
     * @return コンテンツのリスト
     */
	public ArrayList<String> getWebContentList(String url, String enc) {
		Pattern pProp = Pattern.compile("http://ja.dbpedia.org/property/");
		Pattern pRes  = Pattern.compile("http://ja.dbpedia.org/resource/");
		ArrayList<String> resultList = new ArrayList<String>();
		//StringBuffer sb = new StringBuffer();        
        try {          
            URLConnection conn = new URL(url).openConnection();           
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), enc));           
            for (String line = in.readLine(); line != null; line = in.readLine()) {               
                if(!line.equals("\"s\"")&&!line.equals("\"p\"")&&!line.equals("\"o\"")){//変数名でない場合
                	//System.out.println("line="+line);
                	Matcher mP = pProp.matcher(line);
                	Matcher mR = pRes.matcher(line);
                	if(mP.find()){
                		resultList.add(mP.replaceFirst(""));//余分なところを取り除く
                	}
                	else if(mR.find()){
                		resultList.add(mR.replaceFirst(""));//余分なところを取り除く
                	}
                }
            }       
        } catch (IOException e) {          
            e.printStackTrace();      
        }
        //System.out.println("List="+resultList);
        return(resultList);   
    }
	
	
}
