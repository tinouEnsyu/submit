package frame;
import java.util.*;


/**
 * Created by seijihagawa on 2016/11/11.
 */
/*
 AIDemonProcWriteTest.java
  すべての種類のデモン手続きのスーパークラス
*/

/**
 * AIDemonProcWriteTest.javaすべての種類のデモン手続きのスーパークラス
 */
class AIDemonProcWriteTest extends AIDemonProc {

    public
    Object eval(
            AIFrameSystem inFrameSystem,
            AIFrame inFrame,
            String inSlotName,
            Iterator inSlotValues,
            Object inOpts )
    {
    	int i = 0;
    	while(inSlotValues.hasNext()){
    		++i;
    	}
    	System.out.println("length="+i);
        Object obj = AIFrame.getFirst( inSlotValues );
        inFrame.setSlotValue( inSlotName, "hello " + obj );
        return null;
    }

}