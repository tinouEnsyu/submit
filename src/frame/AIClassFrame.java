package frame;

/**
 * Created by seijihagawa on 2016/11/11.
 */
public class AIClassFrame extends AIFrame {

    public AIClassFrame(//コンストラクタ
            AIFrameSystem inFrameSystem,
            AIClassFrame inSuperFrame,
            String inName) {
        super(inFrameSystem, inSuperFrame, inName, false);
    }

}