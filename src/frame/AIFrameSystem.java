package frame;
import java.util.*;


/**
 * Created by seijihagawa on 2016/11/11.
 */

public class AIFrameSystem {

    final static String sTopFrameName = "top_level_frame";

    // すべてのフレームを格納するための辞書．
    // フレーム名をインデックスとして利用．
    private HashMap<String, AIFrame> mFrames = new HashMap<String, AIFrame>();


    /**
     * AIFrameSystem
     * コンストラクタ
     */
    public AIFrameSystem() {
        mFrames.put(sTopFrameName,
                new AIClassFrame(this, null, sTopFrameName));
    }

    public void makeGroupFrame(){//グループの役割分担に関するフレームを生成
        //課題5-2
        //学生のクラスフレーム
        createClassFrame("student");
        //学籍番号のスロット
        writeSlotValue("student","number",new Integer(0));
        //グループメンバーのインスタンスフレーム
        createInstanceFrame("student","kawase");
        createInstanceFrame("student","takada");
        createInstanceFrame("student","naitou");
        createInstanceFrame("student","hagawa");
        createInstanceFrame("student","yosino");
        //学籍番号スロットに値を設定
        writeSlotValue("kawase","number",new Integer(26115049));
        writeSlotValue("takada","number",new Integer(26115080));
        writeSlotValue("naitou","number",new Integer(26115098));
        writeSlotValue("hagawa","number",new Integer(26115110));
        writeSlotValue("yosino","number",new Integer(26115162));
        //グループのクラスフレーム
        createClassFrame("KPgroup10");
        //メンバーのクラスフレーム
        createClassFrame("group10_member");
        //グループのクラスフレームにメンバーのスロットを追加
        writeSlotValue("KPgroup10","member",mFrames.get("group10_member"));
        //メンバーのフレームにメンバーを追加
        writeSlotValue("group10_member","member1",mFrames.get("kawase"));
        writeSlotValue("group10_member","member2",mFrames.get("takada"));
        writeSlotValue("group10_member","member3",mFrames.get("naitou"));
        writeSlotValue("group10_member","member4",mFrames.get("hagawa"));
        writeSlotValue("group10_member","member5",mFrames.get("yosino"));
        //課題5のクラスフレーム
        createClassFrame("task5");
        //グループのクラスフレームに課題のスロットを追加
        writeSlotValue("KPgroup10","task",mFrames.get("task5"));
        //課題のクラスフレームとそのサブクラス課題5-1〜課題5-6
        createClassFrame("subtask");
        writeSlotValue("subtask","number",new Integer(0));
        writeSlotValue("subtask","content",new String(""));
        writeSlotValue("subtask","required",false);
        createInstanceFrame("subtask","subtask1");
        createInstanceFrame("subtask","subtask2");
        createInstanceFrame("subtask","subtask3");
        createInstanceFrame("subtask","subtask4");
        createInstanceFrame("subtask","subtask5");
        createInstanceFrame("subtask","subtask6");
        //課題番号のスロット
        writeSlotValue("subtask1","number",new Integer(1));
        writeSlotValue("subtask2","number",new Integer(2));
        writeSlotValue("subtask3","number",new Integer(3));
        writeSlotValue("subtask4","number",new Integer(4));
        writeSlotValue("subtask5","number",new Integer(5));
        writeSlotValue("subtask6","number",new Integer(6));
        //必須課題かどうか（必須課題ならtrue,そうでないならfalse)
        writeSlotValue("subtask1","required",true);
        writeSlotValue("subtask2","required",true);
        writeSlotValue("subtask3","required",true);
        writeSlotValue("subtask4","required",true);
        writeSlotValue("subtask5","required",false);
        writeSlotValue("subtask6","required",false);
        //課題内容のスロット
        writeSlotValue("subtask1","content",new String("メンバーについてのセマンティックネットの構築"));
        writeSlotValue("subtask2","content",new String("役割分担をフレームで表現"));
        writeSlotValue("subtask3","content",new String("フレームを用いた質問応答システムの作成"));
        writeSlotValue("subtask4","content",new String("グループレポートの作成"));
        writeSlotValue("subtask5","content",new String("関係を図で示すGUIの設計"));
        writeSlotValue("subtask6","content",new String("DBpediaを用いた質問応答システムの作成"));
        //課題5-1〜課題5-6のインスタンスフレームに担当者のスロットを追加
        writeSlotValue("subtask1","charge",mFrames.get("naitou"));
        writeSlotValue("subtask2","charge",mFrames.get("takada"));
        writeSlotValue("subtask3","charge",mFrames.get("yosino"));
        writeSlotValue("subtask4","charge",mFrames.get("kawase"));
        writeSlotValue("subtask5","charge",mFrames.get("hagawa"));

        writeSlotValue("subtask6","charge",mFrames.get("yosino"));
        //課題5のクラスフレームに課題5-1〜課題5-6を追加
        writeSlotValue("task5","subtask1",mFrames.get("subtask1"));
        writeSlotValue("task5","subtask2",mFrames.get("subtask2"));
        writeSlotValue("task5","subtask3",mFrames.get("subtask3"));
        writeSlotValue("task5","subtask4",mFrames.get("subtask4"));
        writeSlotValue("task5","subtask5",mFrames.get("subtask5"));
        writeSlotValue("task5","subtask6",mFrames.get("subtask6"));
        //メンバーのインスタンスフレームに担当課題を追加
        writeSlotValue("kawase","task_charge_of",mFrames.get("subtask4"));
        writeSlotValue("takada","task_charge_of",mFrames.get("subtask2"));
        writeSlotValue("naitou","task_charge_of",mFrames.get("subtask1"));
        writeSlotValue("hagawa","task_charge_of",mFrames.get("subtask5"));
        //writeSlotValue("yosino","task_charge_of",mFrames.get("subtask3"));

        writeSlotValue("yosino","task_charge_of",Arrays.asList(mFrames.get("subtask3"), mFrames.get("subtask6")));
    }

//フレームを生成するためのメソッド群

    /**
     * createClassFrame
     * スーパークラスを持たない クラスフレーム inName を作成する．
     */
    public void createClassFrame(String inName) {
        createFrame(sTopFrameName, inName, false);
    }


    /**
     * createClassFrame
     * スーパーフレームとして inSuperName を持つクラスフレーム
     * inName を作成する．
     *
     * @param inSuperName スーパーフレームのフレーム名
     * @param inName      フレーム名
     */
    public void createClassFrame(String inSuperName, String inName) {
        createFrame(inSuperName, inName, false);
    }


    /**
     * createInstanceFrame
     * スーパーフレームとして inSuperName を持つインスタンスフレーム
     * inName を作成する．
     *
     * @param inSuperName スーパーフレームのフレーム名
     * @param inName      フレーム名
     */
    public void createInstanceFrame(String inSuperName, String inName) {
        createFrame(inSuperName, inName, true);
    }


    /*
     * createFrame
     *  フレームを作成する 全てのcreateInstanceFrame で呼ばれている
     *
     *  @param inSuperName スーパーフレームのフレーム名
     *  @param inName 新たに生成するフレーム名
     *  @param inIsInstance インスタンスフレームなら true
     */
    void createFrame(
            String inSuperName,
            String inName,
            boolean inIsInstance) {
        AIClassFrame frame;
        try {
            frame = (AIClassFrame) mFrames.get(inSuperName);
            createFrame(frame, inName, inIsInstance);
        }
        catch (Throwable err) {
        }
    }


/*
 * createFrame
 *  フレームを作成する
 *
 *  @param inSuperName スーパーフレーム
 *  @param inName フレーム名
 *  @param inIsInstance インスタンスフレームなら true
 */

    void createFrame(
            AIClassFrame inSuperFrame,
            String inName,
            boolean inIsInstance) {
        AIFrame frame;
        if (inIsInstance == true) {
            frame = new AIInstanceFrame(this, inSuperFrame, inName);
        } else {
            frame = new AIClassFrame(this, inSuperFrame, inName);
        }
        mFrames.put(inName, frame);
    }

//スロットから値を取り出すメソッド群

    /**
     * readSlotValue
     * スロット値を返す = スロット値から値を取り出す デフォルト値を優先できる
     *
     * @param inFrameName フレーム名
     * @param inSlotName  スロット名
     * @param inDefault   デフォルト値を優先したいなら true
     */
    public Object readSlotValue(
            String inFrameName,
            String inSlotName,
            boolean inDefault) {
        AIFrame frame = (AIFrame) mFrames.get(inFrameName);//フレームの検索を行う
        //Object obj = frame.readSlotValue(this, inSlotName, inDefault);
        //System.out.println(obj.toString());
        return frame.readSlotValue(this, inSlotName, inDefault);//フレームからスロット値を読み込んで返す
    }


    /**
     * readSlotValue
     * スロット値を返す = スロット値から値を取り出す デフォルト値を優先しない
     *
     * @param inFrameName フレーム名
     * @param inSlotName  スロット名
     */
    public Object readSlotValue(
            String inFrameName,
            String inSlotName) {
        AIFrame frame = (AIFrame) mFrames.get(inFrameName);
        return frame.readSlotValue(this, inSlotName, false);
    }


    /**
     * readSlotValue
     * スロット値を返す  = スロット値から値を取り出す
     *
     * @param inFrameName フレーム名
     * @param inSlotName  スロット名
     * @param inFacetName ファセット名
     */
    public Object readSlotValue(
            String inFrameName,
            String inSlotName,
            String inFacetName) {
        AIFrame frame = (AIFrame) mFrames.get(inFrameName);
        return frame.readSlotValue(this, inSlotName, false);
    }

//スロットに値を設定するメソッド

    /**
     * writeSlotValue
     * スロット値を設定する．
     *
     * @param inFrameName フレーム名
     * @param inSlotName  スロット名
     * @param inSlotValue スロット値
     */
    public void writeSlotValue(
            String inFrameName,
            String inSlotName,
            Object inSlotValue) {
        AIFrame frame = (AIFrame) mFrames.get(inFrameName);//フレームの検索を行う
        frame.writeSlotValue(this, inSlotName, inSlotValue);//検索されたフレームのスロットに新たな値を書き込む
    }


// demon procedure の設定メソッド群 ここから最後まで

    /**
     * setWhenConstructedProc
     * when-constructed procedure を設定する．
     */
    public void setWhenConstructedProc(
            String inFrameName,
            String inSlotName,
            AIWhenConstructedProc inDemonProc) {
        AIFrame frame = (AIFrame) mFrames.get(inFrameName);
        if (frame != null){
            frame.setWhenConstructedProc(inDemonProc);
        }
    }

    public void setWhenConstructedProc(
            String inFrameName,
            String inSlotName,
            String inClassName) {
        try {
            AIWhenConstructedProc demonProc =
                    (AIWhenConstructedProc) Class.forName(inClassName).newInstance();
            AIFrame frame = (AIFrame) mFrames.get(inFrameName);
            if (frame != null){
                frame.setWhenConstructedProc(demonProc);
            }
        } catch (Exception err) {
            System.out.println(err);
        }
    }


    /**
     * setWhenRequestedProc
     * when-requested procedure を設定する．
     */
    public void setWhenRequestedProc(
            String inFrameName,
            String inSlotName,
            AIDemonProc inDemonProc) {
        setDemonProc(AISlot.WHEN_REQUESTED, inFrameName,
                inSlotName, inDemonProc);
    }

    public void setWhenRequestedProcClass(
            String inFrameName,
            String inSlotName,
            String inClassName) {
        setDemonProcClass(AISlot.WHEN_REQUESTED,
                inFrameName, inSlotName, inClassName);
    }


    /**
     * setWhenReadProc
     * when-read procedure を設定する．
     */
    public void setWhenReadProc(
            String inFrameName,
            String inSlotName,
            AIDemonProc inDemonProc) {
        setDemonProc(AISlot.WHEN_READ,
                inFrameName, inSlotName, inDemonProc);
    }

    public void setWhenReadProcClass(
            String inFrameName,
            String inSlotName,
            String inClassName) {
        setDemonProcClass(AISlot.WHEN_READ,
                inFrameName, inSlotName, inClassName);
    }


    /**
     * setWhenWrittenProc
     * when-written procedure を設定する．
     */
    public void setWhenWrittenProc(
            String inFrameName,
            String inSlotName,
            AIDemonProc inDemonProc) {
        setDemonProc(AISlot.WHEN_WRITTEN,
                inFrameName, inSlotName, inDemonProc);
    }

    public void setWhenWrittenProcClass(
            String inFrameName,
            String inSlotName,
            String inClassName) {
        setDemonProcClass(AISlot.WHEN_WRITTEN,
                inFrameName, inSlotName, inClassName);
    }


    /*
     * setDemonProc
     * フレームに demon procedure を設定する．
     */
    void setDemonProc(
            int inType,
            String inFrameName,
            String inSlotName,
            AIDemonProc inDemonProc) {
        AIFrame frame = (AIFrame) mFrames.get(inFrameName);
        if (frame != null)
            frame.setDemonProc(inType, inSlotName, inDemonProc);
    }


    /*
     * setDemonClass
     *  demon procedure を設定する．
     */
    void setDemonProcClass(
            int inType,
            String inFrameName,
            String inSlotName,
            String inClassName) {
        try {
            AIDemonProc demon =
                    (AIDemonProc) Class.forName(inClassName).newInstance();
            setDemonProc(inType, inFrameName, inSlotName, demon);
        } catch (Exception err) {
            System.out.println(err);
        }
    }


    /**
     * フレームを取得する
     * @author takada
     * @param frame 取得するフレームのフレーム名
     * @return AIFrame
     */
    public AIFrame getFrame(String frame){
        return(mFrames.get(frame));
    }

    //引数に与えられたクラスの全インスタンスの名前を返す
    public ArrayList<String> searchClass(String classFrameName){
        ArrayList<String> nameList = new ArrayList<String>();
        Set<String>keyS = mFrames.keySet();//キーを入手
        for(String key : keyS){//全てのキーを調べる
            AIFrame frame = mFrames.get(key);//キーに対応するフレームを入手する
            if(frame.isInstance()){//フレームがインスタンスである場合
                Iterator<AIClassFrame> list = frame.getSupers();//フレームのスーパークラスを入手
                while(list.hasNext()){//全てのスーパークラスを調べる
                    String superName = list.next().getName();//スーパークラスの名前を入手
                    if(superName.equals(classFrameName)){//引数のクラス名と一致した場合
                        //System.out.println("super="+superName+" frame="+frame.getName());
                        nameList.add(frame.getName());//リストに追加する
                        break;
                    }
                }
            }
        }
        return(nameList);
    }

} // end of class definition