package frame;
import java.util.*;


/**
 * Created by seijihagawa on 2016/11/11.
 */

/**
 *  AIDemonProcReadTest.java
 *  すべての種類のデモン手続きのスーパークラス
 *  when-read procedure は，スロット値を Iterator として
 *  返さなけらばならない
 */
class AIDemonProcReadTest extends AIDemonProc {

    public
    Object eval(
            AIFrameSystem inFrameSystem,
            AIFrame inFrame,
            String inSlotName,
            Iterator inSlotValues,
            Object inOpts )
    {
        Object height = inFrame.readSlotValue( inFrameSystem, "height", false );
        if ( height instanceof Integer ) {//height が Integer クラスのオブジェクトである場合
            int h = ((Integer) height).intValue();
            return AIFrame.makeEnum( new Integer( (int) (0.9 * (h - 100))) );
        }
        return null;
    }

}