package frame;
import java.util.*;

public class QandASystem {
	final Map<String, String> WordConvertMapMulti =
		    new HashMap<String, String>() {{
		  put("学生","student");put("課題", "subtask");
		}};
	
	final Map<String, String> WordConvertMapFrame =
		    new HashMap<String, String>() {{
		  put("学生","student");
		  put("課題", "task5");put("グループ","group10_member");
		  put("川瀬", "kawase");put("高田", "takada");put("内藤", "naitou");put("羽川", "hagawa");put("芳野", "yosino");
		  put("5-1", "subtask1");put("5-2", "subtask2");put("5-3", "subtask3");put("5-4", "subtask4");put("5-5", "subtask5");put("5-6", "subtask6");
		}};
	final Map<String, String> WordConvertMapSlot =
			    new HashMap<String, String>() {{
			  put("課題", "task_charge_of");put("必須","required");put("番号","number");
			  put("5-1", "subtask1");put("5-2", "subtask2");put("5-3", "subtask3");put("5-4", "subtask4");put("5-5", "subtask5");put("5-6", "subtask6");
			  put("１人目", "member1");put("２人目", "member2");put("３人目", "member3");put("４人目", "member4");put("５人目", "member5");
			  put("学籍番号","number");put("担当者","charge");put("内容","content");put("担当","task_charge_of");
		}};
		
	public static void main(String[] args){
		//AIFrameSystem fs = new AIFrameSystem();
		groupFrame fs = new groupFrame();
		//fs.makeGroupFrame();
		new QandASystem().getAnswerExtend(args[0],fs);
	}
	
	//主語（代名詞でない）を含む文節を探す
	Chunk getSubjectChunk(Sentence sentence){
		int i = 0;
		Chunk subChunk = sentence.get(i);
		Morpheme subMorph = subChunk.get(0);
		while(subMorph.contain("代名詞")){//代名詞からで始まらなくなるまで続ける
			++i;
			subChunk = sentence.get(i);
			subMorph = subChunk.get(0);
		}
		
		return(subChunk);
		
	}
	
	//与えられた文節から、その文章で問われていること修飾する（答えを制限する）言葉を求める
	ArrayList<String> getModification(Chunk arg_subject, String arg_target){
		ArrayList<String> modifS = new ArrayList<String>();
		Chunk dependent = arg_subject;
		while(true){
			String modif = "";
			dependent = dependent.dependencyChunk;
			int i = 0;
			while(i < dependent.size()){//文節中の全単語を調べる
				Morpheme morph_i = dependent.get(i); // i 番目の単語を入手する
				if(morph_i.contain("名詞")){//名詞なら連続で追加
					modif = modif.concat(morph_i.getSurface());
				}
				else if(i == 0){//初回が名詞以外ならそれだけ追加して終了
					modif = modif.concat(morph_i.getSurface());
					break;
				}
				
				++i;
			}
			
			if(modif.equals(arg_target)){//目的語の文節に達した場合
				break;//脱出 終了
			}
			else{//達していない場合
				modifS.add(modif);//リストに追加する
			}
			//System.out.println("sub="+dependent);
		}//while(true)
		return(modifS);
	}
	
	//与えられた主辞の文節から、その文章で問われていることを求める 
	String getQuestion(Chunk tail){
		String question = "";
		Chunk new_tail = tail;//文節をコピーする
		System.out.println("tail="+tail.getSurface());
		if(tail.getSurface().equals("？") || tail.getSurface().equals("?")){//疑問符だけだった場合
			List<Chunk> dependents = tail.getDependents();
			new_tail = dependents.get(dependents.size()-1);//それに係る文節の一番後ろを入手
			//System.out.println("new_tail="+new_tail.getSurface());
		}
		  
		int i = 0;
		while(i < new_tail.size()){//文節中の全単語を調べる
			Morpheme morph_i = new_tail.get(i); // i 番目の単語を入手する
			if(morph_i.contain("名詞")){//名詞を含んでいるなら追加
				if(morph_i.contain("代名詞")){//その名詞が代名詞であった場合
					List<Chunk> dependents = tail.getDependents();
					question = getQuestion(dependents.get(dependents.size()-1));//それに係る文節の一番後ろの文節から名詞をもう一度探す
				}
				else{
					question = question.concat(morph_i.getSurface());
				}
			}
			else{//そうでないなら終了
				break;
			}
			++i;
		}
		      
		return(question);
	}
	
	//複数の答えを探す
	void getAnswerMulti(String arg_target,AIFrameSystem arg_fs){
		String conv_t = WordConvertMapMulti.get(arg_target);//変換を行う
		ArrayList<String> ansList = arg_fs.searchClass(conv_t);//答えのリストを求める
		if(ansList.size() > 0){
			for(String ans : ansList){
				System.out.println(ans);
			}
		}
		else{
			System.out.println("すいません。見つかりませんでした。");
		}
	}
	
	//イテレーターの中身を表示する
	void showIterator(Iterator ite){
		Object obj = ite.next();
		if(obj instanceof AIFrame){
			AIFrame mini_frame = (AIFrame)obj;
			System.out.println(mini_frame.getName());
			while(ite.hasNext()){
				mini_frame = (AIFrame)ite.next();
				System.out.println(mini_frame.getName());
			}
		}
		else{
			System.out.println(obj);
			while(ite.hasNext()){
				System.out.println(ite.next());
			}
		}
		
	}
	
	
	void getAnswerFinal(String arg_frame,String arg_slot, AIFrameSystem arg_fs){
		if(!arg_frame.equals(null) && !arg_slot.equals(null)){
      		Object read = arg_fs.readSlotValue(arg_frame, arg_slot,false);
      		if(read instanceof Iterator || read instanceof List){//イテレーターであった場合
      			if(read instanceof Iterator){
      				showIterator((Iterator)read);
      			}
      			else{
      				List list = (List)read;
      				showIterator(list.iterator());
      			}
      		}
      		else if(read instanceof String || read instanceof Integer || read instanceof Boolean){//フレームでない単体にたどり着いた場合
    				System.out.println(read);
    		}
    		else{//単体のフレームであった場合
    			AIFrame mini_frame = (AIFrame)arg_fs.readSlotValue(arg_frame, arg_slot);
    			String frameName = mini_frame.getName();//名前を入手する
    			System.out.println(mini_frame.getName());//名前を表示する
    		}
  		}
  		else{
  			System.out.println("すいません、分かりません。");
  		}
	}
	
	void getAnswer(String question,AIFrameSystem arg_fs){
		System.out.println("質問 = "+question);
		List<Sentence> sentences = Sentence.parseTweet(question);//係り受け解析した結果(Sentenceのリスト)を受け取る
		System.out.println("size="+sentences.size());//係り受け解析結果を表示する
		Sentence sentence = sentences.get(0);//基本的に文は１つだけなので先頭を入手する。
		//System.out.println("Last="+sentence.getHeadChunk().getSurface());
		//String subject = sentence.get(0).get(0).getSurface();
		//subject = getModification(sentence.get(0));
		Chunk subChunk = getSubjectChunk(sentence);
		String subject = subChunk.get(0).getSurface();
		Chunk tail = sentence.getHeadChunk();//その文の主辞文節を入手
		String target = getQuestion(tail);
		if(subject.equals(target)){//主語と目的が同じ　なほど短い文だった場合
			System.out.println("subject= "+subject+" target="+target);
			getAnswerMulti(target,arg_fs);
		}
		else{
			//ArrayList<String> modificationS = getModification(sentence.get(0),target);
			System.out.println("subject= "+subject+" target="+target);
			ArrayList<String> modificationS = getModification(subChunk,target);
			System.out.println("subject= "+subject+" modifyList="+modificationS+" target="+target);
			//System.out.println("target2="+Morpheme.analyzeMorpheme(target));
			String frame = "",slot = "";
			if(modificationS.size() == 0){//修飾がない場合
				frame = WordConvertMapFrame.get(subject);
				slot = WordConvertMapSlot.get(target);
				getAnswerFinal(frame,slot,arg_fs);
			}
			else{//修飾があった場合
				frame = WordConvertMapFrame.get(subject);
				int i = 0;
				while(i <= modificationS.size()){
					if(i == modificationS.size()){//全ての修飾を使い切った場合(初回{i = 0}でここに来ることはあり得ない)
						slot = WordConvertMapSlot.get(target);
						getAnswerFinal(frame,slot,arg_fs);
					}
					else{//修飾がまだ残っている場合
						slot = WordConvertMapSlot.get(modificationS.get(i));
						if(!frame.equals(null) && !slot.equals(null)){
							Object read = arg_fs.readSlotValue(frame, slot,false);
							if(read instanceof String || read instanceof Integer || read instanceof Boolean){//フレームでなく単体にたどり着いた場合
								System.out.println(read);
							}
							else{//フレームであった場合
								AIFrame mini_frame = (AIFrame)arg_fs.readSlotValue(frame, slot);//フレームを入手
								String frameName = mini_frame.getName();//名前を入手
								frame = frameName;//次の探索に使えるようにする
							}
						}
						else{
							System.out.println("すいません、分かりません。");
						}
			          			}//else 修飾がまだ残っている場合
					++i;
				}//while
			}//else 修飾があった場合
		}//else
	}	

	void getAnswerExtend(String question, AIFrameSystem arg_fs){
		boolean taskMode = false;
		ArrayList<Morpheme> morphs = Morpheme.analyzeMorpheme(question); // 形態素解析した結果（Morphemeのリスト）を受け取る
		for(Morpheme morph : morphs){
			String word = morph.getSurface();
			if(WordConvertMapMulti.containsKey(word)||WordConvertMapFrame.containsKey(word)||WordConvertMapSlot.containsKey(word)){//今回の課題に関連すると思われる言葉が含まれている場合
				taskMode = true;//課題モードとする
				break;
			}
		}
		
		if(taskMode){//課題モードの場合
			new QandASystem().getAnswer(question, arg_fs);
		}
		else{//
			new DBpediaSearch().getAnswerS(question);
		}
	}
}
