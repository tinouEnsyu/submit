package frame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

//グループの役割分担に関するフレームを生成するクラス
public class groupFrame extends AIFrameSystem{

	//コンストラクタ
	groupFrame(){
		//クラスフレームをファイルから読み込む
		readClass();
		//インスタンスフレームをファイルから読み込む
		readInstance();
		//スロットをファイルから読み込む
		readSlot();
	}

	/**
	 * クラスフレームをファイルから読み込む.
	 */
	void readClass(){
		try{
			//ファイルを読み込む
			//File file = new File("Morpheme.java");
			File file = new File("src/db/frame_class.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			//読み込んだファイルを1行ずつ処理する
			String line;
			while((line = br.readLine()) != null){
				//クラスフレームを生成する
				createClassFrame(line);
			}
			br.close();

		}//例外処理
		catch(FileNotFoundException e){
			System.out.println(e);
		}
		catch(IOException e){
			System.out.println(e);
		}
	}

	/**
	 * インスタンスフレームをファイルから読み込む.
	 */
	void readInstance(){
		try{
			//ファイルを読み込む
			File file = new File("src/db/frame_instance.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			//読み込んだファイルを1行ずつ処理する
			String line;
			StringTokenizer token;
			String Super;
			String Sub;
			while((line = br.readLine()) != null){
				//","で分割する
				token = new StringTokenizer(line,",");

				//2つのtokenを読む
				Super = token.nextToken();
				Sub = token.nextToken();

				//インスタンスフレームを生成
				createInstanceFrame(Super,Sub);
			}
			br.close();

		}//例外処理
		catch(FileNotFoundException e){
			System.out.println(e);
		}
		catch(IOException e){
			System.out.println(e);
		}
	}

	/**
	 * スロットをファイルから読み込む.
	 */
	void readSlot(){
		try{
			//ファイルを読み込む
			File file = new File("src/db/frame_slot.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			//読み込んだファイルを1行ずつ処理する
			String line;
			StringTokenizer token;
			String fname;
			String sname;
			String type;
			String data;
			while((line = br.readLine()) != null){
				//","で分割する
				token = new StringTokenizer(line,",");

				//4つのtokenを読む
				fname = token.nextToken();
				sname = token.nextToken();
				type = token.nextToken();
				data = token.nextToken();

				//スロットを追加
				if(type.equals("int")){
					writeSlotValue(fname,sname,Integer.parseInt(data));
				}
				else if(type.equals("string")){
					writeSlotValue(fname,sname,data);
				}
				else if(type.equals("boolean")){
					if(data.equals("true")){
						writeSlotValue(fname,sname,true);
					}
					else if(data.equals("false")){
						writeSlotValue(fname,sname,false);
					}
				}
				else if(type.equals("frame")){
					writeSlotValue(fname,sname,getFrame(data));
				}
				else if(type.equals("null")){
					writeSlotValue(fname,sname,null);
				}
			}
			br.close();

		}//例外処理
		catch(FileNotFoundException e){
			System.out.println(e);
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
}