package semanticNet;
import java.util.*;
import java.io.*;

/***
 * グループ５人のSemanticNetの構築
 * まずcsvファイルを開き一行ずつ読み込む.
 * csvファイルには,一行ごとにセマンティックネットのリンク関係を表す情報を入れておく.
 * その読み込まれたものをsplitを使い","で分ける.
 * 分けられたものをaddLinkでリンク関係として追加する.
 * ファイルを最後まで読み込んだところでループを抜ける.
 * @author naito
 * @see SemanticNet#addLink(Link)
 * @see SemanticNet#printLinks()
 * @see SemanticNet#printNodes()
 * @see SemanticNet#query(ArrayList)
 * @see SemanticNet#printLinks()
 * @see SemanticNet#printNodes()
 */
public class Example {
	public static void main(String args[]){
		SemanticNet sn = new SemanticNet();
		try{
			File file = new File("semanticNet.csv");
			FileReader reader = new FileReader(file);
			BufferedReader br = new BufferedReader(reader);
			String str = new String();
			str = br.readLine();
			String[] array;

			while(str != null){
				array = str.split(",",0);
				sn.addLink(new Link(array[0], array[1], array[2], sn));
				str = br.readLine();
			}
			br.close();

			//sn.printLinks();
			//sn.printNodes();

			ArrayList<Link> query = new ArrayList<Link>();
	        query.add(new Link("from", "?y", "Aichi"));
	        sn.query(query);
			
		}catch(FileNotFoundException e){
			System.out.println(e);
		}catch(IOException e){
			System.out.println(e);
		}
	}
}